import {BaseLlm} from "./base-llm";

import {Ollama, OllamaInput} from "@langchain/community/llms/ollama";
import { OllamaFunctions } from "langchain/experimental/chat_models/ollama_functions";
import {LlmParamsType} from "../types/llm-params-type";
import { convertToOpenAIFunction } from "@langchain/core/utils/function_calling";
import {BaseTool} from "../tools/base-tool";
import {DynamicStructuredTool} from "langchain/tools";
import {HumanChatMessage} from "langchain/schema";

export class OllamaLlm extends BaseLlm {
    protected llm: OllamaFunctions

    constructor(params: LlmParamsType) {
        super(params);
        this.llm = new OllamaFunctions(params);
    }

    addContext(context: any): OllamaLlm {
        return this;
    }

    bindToools(tools: BaseTool[]) {
        // const openaiTools: DynamicStructuredTool[] = tools.map((tool)=> {
        //     const instance = tool.build();
        //     return instance
        // })
        const  funcs = [
            {
                name: "get_order_info",
                description: "Get the current order info in a given number",
                parameters: {
                    type: "object",
                    properties: {
                        order_id: {
                            type: "string",
                            description: "The city and state, e.g. San Francisco, CA",
                        },
                    },
                    required: ["order_id"],
                },
            },
        ]
        this.llm.bind({functions: funcs})
    }

    public async invokeMessage(message: string) {
        const response = await this.llm.invoke([
            new HumanChatMessage({
                content: message,
            }),
        ])
    }


}