import {OllamaInput} from "@langchain/community/llms/ollama";
import {BaseLLMParams} from "@langchain/core/language_models/llms";

export type LlmParamsType = Record<string, any> & OllamaInput & BaseLLMParams