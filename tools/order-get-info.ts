import {DynamicTool} from "@langchain/core/tools";
import {BaseTool} from "./base-tool";
import {DynamicStructuredTool} from "langchain/tools";
import {z} from "zod";

const customTool = new DynamicTool({
    name: "get_order_info",
    description: "Returns order ino",
    func: async (input: string) => input.length.toString(),
});

// noinspection TypeScriptValidateTypes
export class OrderGetInfo extends BaseTool {
    protected name: string = 'get_order_info';
    protected description: string = 'returns order info by order number';

    constructor() {
        super();
    }

    build() {
        return {}
        // new DynamicStructuredTool({
        //     description: this.description,
        //     func(input: any): Promise<string> {
        //         return Promise.resolve("");
        //     },
        //     name: this.name,
        //     schema: undefined
        //     // ,
        //     // metadata: undefined,
        //     // name: "",
        //     // returnDirect: false,
        //     // schema: undefined,
        //     // tags: [],
        //     // verbose: false
        // })
    }
}