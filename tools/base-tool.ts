import {DynamicTool} from "@langchain/core/tools";
import {DynamicStructuredTool} from "langchain/tools";

export abstract class BaseTool {
    protected abstract name: string;
    protected abstract description: string;
    abstract build(): Record<string, any>

}