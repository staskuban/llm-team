import {OllamaLlm} from "./llm/ollama-llm";

import {LlmParamsType} from "./types/llm-params-type";
import 'dotenv/config'

import { Telegraf } from 'telegraf'
import { message } from 'telegraf/filters'

export class Runner {
    private llm!: OllamaLlm
    private bot!: Telegraf

    constructor() {
    }

    private createLLM() {
        const params: LlmParamsType = {
            baseUrl: process.env.MODEL_URL,
            model: process.env.MODEL_NAME,
            temperature:0.1
        };

        console.info(`preparing llm with params ${JSON.stringify(params)}`)
        this.llm = new OllamaLlm(params);
        console.info('llm ready')
    }


    private createBot() {
        this.bot = new Telegraf('6863653888:AAEcYxliK0743P1ZAfUrviOVLUzePvuv_ds');
        // noinspection TypeScriptValidateTypes
        this.bot.on(message('text'), async (ctx) => {


            if (this.llm instanceof OllamaLlm) {
                const result = await this.llm.invokeMessage(ctx.message.text);
                console.log(result)
            }

            await ctx.reply(`Hello dfdf`)
        })
        this.bot.launch();
        process.once('SIGINT', () => this.bot.stop('SIGINT'));
        process.once('SIGTERM', () => this.bot.stop('SIGTERM'));
    }
    public run() {
        this.createLLM();
        this.createBot();

    }

}

const instance = new Runner();
instance.run();